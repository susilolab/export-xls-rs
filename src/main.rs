use simple_excel_writer as excel;
use excel::*;

fn main() {
    let mut wb = Workbook::create("/tmp/b.xlsx");
    let mut sheet = wb.create_sheet("Sheet1");
    let data = vec![
        [Row::new().add_cell("No"), Row::new().add_cell("Name")],
        [Row::new().add_cell(1.0), Row::new().add_cell("Agus")],
    ];
    
    sheet.add_column(Column { width: 10.0 });
    sheet.add_column(Column { width: 30.0 });
    wb.write_sheet(&mut sheet, |sw| {
        sw.append_row(Row::new().add_cell(vec![1.0]))
    }).expect("write excel error!.");

    wb.close().expect("close excel error");
}
